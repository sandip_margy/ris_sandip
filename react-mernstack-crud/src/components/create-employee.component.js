import React, { Component } from "react";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';
import axios from 'axios';

export default class CreateEmployee extends Component {

  constructor(props) {
    super(props)

    // Setting up functions 
    this.onChangeEmployeeFirstName = this.onChangeEmployeeFirstName.bind(this);
    this.onChangeEmployeeLastName = this.onChangeEmployeeLastName.bind(this);
    this.onChangeEmployeeEmail = this.onChangeEmployeeEmail.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    // Setting up state
    this.state = {
      fname: '',
      lname: '',
      email: '',
      
    }
  }


  onChangeEmployeeFirstName(e) {
    this.setState({ fname: e.target.value })
  }

  onChangeEmployeeLastName(e) {
    this.setState({ lname: e.target.value })
  }

  onChangeEmployeeEmail(e) {
    this.setState({ email: e.target.value })
  }

  onSubmit(e) {
    e.preventDefault()

    const employeeObject = {
      fname: this.state.fname,
      lname: this.state.lname,
      email: this.state.email,
      
    };
    axios.post('http://localhost:4000/employee/create-employee', employeeObject)
      .then(res => console.log(res.data));

    this.setState({ fname: '',lname: '',email:''})
  }

  render() {
    return (<div className="form-wrapper">
      <Form onSubmit={this.onSubmit}>
        <Form.Group controlId="FName" >
          <Form.Label>First Name</Form.Label>
          <Form.Control type="text" value={this.state.fname} onChange={this.onChangeEmployeeFirstName} required/>
        </Form.Group>

        <Form.Group controlId="LName" >
          <Form.Label>Last Name</Form.Label>
          <Form.Control type="text" value={this.state.lname} onChange={this.onChangeEmployeeLastName}/>
        </Form.Group>


        <Form.Group controlId="Email">
          <Form.Label>Email</Form.Label>
          <Form.Control type="email" value={this.state.email} onChange={this.onChangeEmployeeEmail} />
        </Form.Group>


        <Button variant="danger" size="lg" block="block" type="submit" className="mt-4">
          Create Employee
        </Button>
      </Form>
    </div>);
  }
}