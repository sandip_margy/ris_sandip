import React, { Component } from "react";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';
import axios from 'axios';


export default class EditEmployee extends Component {

  constructor(props) {
    super(props)

    this.onChangeEmployeeFirstName = this.onChangeEmployeeFirstName.bind(this);
    this.onChangeEmployeeLastName = this.onChangeEmployeeLastName.bind(this);
    this.onChangeEmployeeEmail = this.onChangeEmployeeEmail.bind(this);
    this.onSubmit = this.onSubmit.bind(this);


    // State
    this.state = {
      fname: '',
      lname: '',
      email: '',
    }
  }

  componentDidMount() {
    axios.get('http://localhost:4000/employee/edit-employee/' + this.props.match.params.id)
      .then(res => {
        this.setState({
          fname: res.data.fname,
          lname: res.data.lname,
          email: res.data.email,
        });
      })
      .catch((error) => {
        console.log(error);
      })
  }

  

  onChangeEmployeeFirstName(e) {
    this.setState({ fname: e.target.value })
  }

  onChangeEmployeeLastName(e) {
    this.setState({ lfname: e.target.value })
  }

  onChangeEmployeeEmail(e) {
    this.setState({ email: e.target.value })
  }
  

  onSubmit(e) {
    e.preventDefault()

    const employeeObject = {
      fname: this.state.fname,
      lname: this.state.lname,
      email: this.state.email,
      
    };

    axios.put('http://localhost:4000/employee/update-employee/' + this.props.match.params.id, employeeObject)
      .then((res) => {
        console.log(res.data)
        console.log('Employee successfully updated')
      }).catch((error) => {
        console.log(error)
      })

    // Redirect to Employee List 
    this.props.history.push('/employee-list')
  }


  render() {
    return (<div className="form-wrapper">
    <Form onSubmit={this.onSubmit}>

    <Form.Group controlId="FName" >
      <Form.Label>First Name</Form.Label>
      <Form.Control type="text" value={this.state.fname} onChange={this.onChangeEmployeeFirstName} required/>
    </Form.Group>

    <Form.Group controlId="LName" >
      <Form.Label>Last Name</Form.Label>
      <Form.Control type="text" value={this.state.lname} onChange={this.onChangeEmployeeLastName} required/>
    </Form.Group>
    <Form.Group controlId="Email">
      <Form.Label>Email</Form.Label>
      <Form.Control type="email" value={this.state.email} onChange={this.onChangeEmployeeEmail} />
    </Form.Group> 


    <Button variant="danger" size="lg" block="block" type="submit" className="mt-4">
      Update Employee
    </Button>
  </Form>
    </div>);
  }
}
