import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import Button from 'react-bootstrap/Button'

export default class EmployeeTableRow extends Component {
  constructor(props) {
    super(props)
    this.deleteEmployee = this.deleteEmployee.bind(this)
  }

  deleteEmployee() {
    axios
      .delete(
        'http://localhost:4000/employee/delete-employee/' + this.props.obj._id,
      )
      .then((res) => {
        console.log('Employee successfully deleted!')
      })
      .catch((error) => {
        console.log(error)
      })
  }

  render() {
    return (
      <tr>
        <td>{this.props.obj.fname}</td>
        <td>{this.props.obj.lname}</td>
        <td>{this.props.obj.email}</td>
        
        <td>
          <Link
            className="edit-link edits" path={"product/:id"}
            to={'/edit-employee/' + this.props.obj._id} variant="primary"
          >
            Edit
          </Link>
          <Button onClick={this.deleteEmployee} size="sm" variant="secondary">
            Delete
          </Button>
        </td>
      </tr>
    )
  }
}