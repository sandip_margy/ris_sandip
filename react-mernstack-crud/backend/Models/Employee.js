const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let employeeSchema = new Schema({
  fname: {
    type: String
  },
  lname: {
    type: String
  },
  email: {
    type: String
  },
  
}, {
    collection: 'employee'
  })

module.exports = mongoose.model('Employee', employeeSchema)